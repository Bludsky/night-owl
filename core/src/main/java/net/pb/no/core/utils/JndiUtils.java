package net.pb.no.core.utils;

import org.springframework.jndi.JndiLookupFailureException;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;

public final class JndiUtils {

    private static final String LOCATION_PREFIX = "java:comp/env/";

    private static final JndiTemplate JNDI_TEMPLATE = new JndiTemplate();

    private JndiUtils() {

    }

    private Object readFromJndi(String location) {
        try {
            return JNDI_TEMPLATE.lookup(LOCATION_PREFIX.concat(location));
        } catch (NamingException e) {
            throw new JndiLookupFailureException("Jndi resource not found", e);
        }
    }


}
