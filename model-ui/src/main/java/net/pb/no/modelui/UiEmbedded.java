package net.pb.no.modelui;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class UiEmbedded<T> extends ResourceSupport {

    private List<T> content;

    @JsonCreator
    public UiEmbedded(@JsonProperty("_embedded") List<T> content) {
        this.content = content;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
