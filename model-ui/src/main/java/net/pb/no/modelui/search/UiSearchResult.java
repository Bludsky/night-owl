package net.pb.no.modelui.search;

public class UiSearchResult {

    private String title;

    private String sanitizedTitle;

    public UiSearchResult() {
    }

    public UiSearchResult(String title, String sanitizedTitle) {
        this.title = title;
        this.sanitizedTitle = sanitizedTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSanitizedTitle() {
        return sanitizedTitle;
    }

    public void setSanitizedTitle(String sanitizedTitle) {
        this.sanitizedTitle = sanitizedTitle;
    }
}
