package net.pb.no.modelui.tag;

public class UiTag {

    public UiTag() {
    }

    public UiTag(String name, String color, Integer occurrence) {
        this.name = name;
        this.color = color;
        this.occurrence = occurrence;
    }

    private String name;

    private String color;

    private Integer occurrence;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getOccurrence() {
        return occurrence;
    }
}
