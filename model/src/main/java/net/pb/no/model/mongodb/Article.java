package net.pb.no.model.mongodb;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document
public class Article extends BaseEntity {

    private String title;

    @Indexed(unique = true)
    private String sanitizedTitle;

    private String description;

    private String author;

    private String sanitizedAuthor;

    private LocalDateTime created = LocalDateTime.now();

    private boolean visible = true;

    private List<Tag> tags;

    private List<Comment> comments;

    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSanitizedTitle() {
        return sanitizedTitle;
    }

    public void setSanitizedTitle(String sanitizedTitle) {
        this.sanitizedTitle = sanitizedTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSanitizedAuthor() {
        return sanitizedAuthor;
    }

    public void setSanitizedAuthor(String sanitizedAuthor) {
        this.sanitizedAuthor = sanitizedAuthor;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
