package net.pb.no.model.mongodb;

import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;
import java.util.List;

@Projection(name = "featureList", types = Article.class)
public interface ArticleFeatureListProjection {

    String getTitle();

    String getSanitizedTitle();

    String getDescription();

    String getAuthor();

    String getSanitizedAuthor();

    LocalDateTime getCreated();

    List<Tag> getTags();

}
