package net.pb.no.model.mongodb;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "simpleList", types = Article.class)
public interface ArticleSimpleListProjection {

    String getTitle();

    String getSanitizedTitle();

    String getAuthor();

}
