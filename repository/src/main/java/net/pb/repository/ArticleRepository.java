package net.pb.repository;

import net.pb.no.model.mongodb.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "articles", path = "articles")
public interface ArticleRepository extends MongoRepository<Article, String> {

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    Article findOne(String id);

    @PreAuthorize("hasRole('ADMIN')")
    Article findBySanitizedTitle(@Param("sanitizedTitle") String sanitizedTitle);

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    Article save(Article article);

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    void delete(String s);

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    void delete(Article article);

    @RestResource(exported = false)
    @Override
    void delete(Iterable<? extends Article> iterable);

    @RestResource(exported = false)
    @Override
    void deleteAll();

    // Prevents GET /articles (would return visible=false as well)
    @RestResource(exported = false)
    Page<Article> findAll(Pageable pageable);

    Article findBySanitizedTitleAndVisibleIsTrue(@Param("sanitizedTitle") String sanitizedTitle);

    List<Article> findByVisibleIsTrueOrderByCreated();

    Page<Article> findByVisibleIsTrue(Pageable pageable);

    Page<Article> findByVisibleIsTrueAndTagsNameIn(@Param("tags") String[] tags, Pageable pageable);

    List<Article> findBySanitizedAuthorAndVisibleIsTrue(@Param("sanitizedAuthor") String sanitizedAuthor);
}
