package net.pb.repository.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import net.pb.repository.converter.LocalDateTimeToLongConverter;
import net.pb.repository.converter.LongToLocalDateTimeConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;

@PropertySource("classpath:mongodb.properties")
@EnableMongoRepositories("net.pb.repository")
@ComponentScan("net.pb.repository.handler")
@Configuration
public class RepositoryConfig extends AbstractMongoConfiguration {

    @Value("${mongodb.host}")
    private String host;

    @Value("#{T(java.lang.Integer).parseInt('${mongodb.port}')}")
    private Integer port;

    @Value("${mongodb.db}")
    private String dbName;

    @Value("${mongodb.model.package}")
    private String mappingBasePackage;

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    protected String getMappingBasePackage() {
        return mappingBasePackage;
    }

    @Bean
    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(host, port);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), dbName);
    }

    @Bean
    @Override
    public CustomConversions customConversions() {
        return new CustomConversions(
                Arrays.asList(
                        new LocalDateTimeToLongConverter(),
                        new LongToLocalDateTimeConverter()
                ));
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
