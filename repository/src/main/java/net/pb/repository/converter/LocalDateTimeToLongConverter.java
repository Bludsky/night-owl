package net.pb.repository.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeToLongConverter implements Converter<LocalDateTime, Long> {

    public Long convert(LocalDateTime localDateTime) {
        return localDateTime.toEpochSecond(ZoneOffset.UTC);
    }

}
