package net.pb.repository.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LongToLocalDateTimeConverter implements Converter<Long, LocalDateTime> {

    public LocalDateTime convert(Long aLong) {
        return LocalDateTime.ofEpochSecond(aLong, 0, ZoneOffset.UTC);
    }

}
