package net.pb.repository.handler;

import net.pb.no.model.mongodb.Article;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Article.class)
public class ArticleRepositoryEventHandler {

    @HandleBeforeCreate
    @HandleBeforeSave
    public void handleBeforeDelete(Article article) {
        String sanitizedTitle = sanitize(article.getTitle());
        String sanitizedAuthor = sanitize(article.getAuthor());

        article.setSanitizedTitle(sanitizedTitle);
        article.setSanitizedAuthor(sanitizedAuthor);
    }

    private String sanitize(String str) {
        return str.replaceAll("(\\W+)", "-").toLowerCase();
    }

}
