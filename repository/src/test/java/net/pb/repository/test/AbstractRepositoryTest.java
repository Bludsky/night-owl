package net.pb.repository.test;

import net.pb.repository.ArticleRepository;
import net.pb.repository.config.RepositoryConfig;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RepositoryConfig.class)
public abstract class AbstractRepositoryTest {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRepositoryTest.class);

    @Autowired
    protected ArticleRepository articleRepository;

    @Before
    public void setUp() {
        cleanRepositories();
    }

    protected void cleanRepositories() {
        LOG.info("Cleaning repositories prior running the test");
        articleRepository.deleteAll();
    }

}
