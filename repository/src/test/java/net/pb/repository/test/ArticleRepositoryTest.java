package net.pb.repository.test;

import net.pb.no.model.mongodb.Article;
import net.pb.no.model.mongodb.Tag;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;

import static net.pb.repository.test.util.RepositoryTestModelFactory.createArticle;
import static net.pb.repository.test.util.RepositoryTestModelFactory.createArticleWithSanitizedTitle;
import static net.pb.repository.test.util.RepositoryTestModelFactory.createTag;
import static org.hamcrest.CoreMatchers.is;

public class ArticleRepositoryTest extends AbstractRepositoryTest {

    @Test
    public void emptyRepositoryTest() {
        Assert.assertThat(articleRepository
                .findAll()
                .iterator()
                .hasNext(), is(false));
    }

    @Test
    public void listVisibleOnlyTest() {
        Article articleOne = createArticleWithSanitizedTitle("Test Title 1", true);
        Article articleTwo = createArticleWithSanitizedTitle("Test Title 2", false);
        Article articleThree = createArticleWithSanitizedTitle("Test Title 3", true);

        articleRepository.save(articleOne);
        articleRepository.save(articleTwo);
        articleRepository.save(articleThree);

        Assert.assertThat(articleRepository
                        .findByVisibleIsTrue(new PageRequest(0, 3))
                        .getTotalElements(),
                is(2l));
    }

    @Test(expected = DuplicateKeyException.class)
    public void uniqueTitleIndexTest() {
        Article articleOne = createArticleWithSanitizedTitle("Test Title", true);
        Article articleTwo = createArticleWithSanitizedTitle("Test Title", true);

        articleRepository.save(articleOne);
        articleRepository.save(articleTwo);
    }

    @Test
    public void tagTest() {
        Article article = createArticle("Test Title", true);

        Tag javaTag = createTag("Java");
        Tag pythonTag = createTag("Python");

        article.setTags(Arrays.asList(javaTag, pythonTag));

        articleRepository.save(article);

        final PageRequest pageRequest = new PageRequest(0, 10);
        Page<Article> articles = articleRepository.findByVisibleIsTrueAndTagsNameIn(new String[]{"Java"}, pageRequest);
        Assert.assertThat(articles.getTotalElements(), is(1L));
    }

}
