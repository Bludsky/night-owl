package net.pb.repository.test.util;

import net.pb.no.model.mongodb.Article;
import net.pb.no.model.mongodb.Tag;

import java.time.LocalDateTime;

public final class RepositoryTestModelFactory {

    private static int counter = 0;

    private RepositoryTestModelFactory() {

    }

    public static Article createArticle(String title, boolean visible) {
        Article article = new Article();
        article.setTitle(title);
        article.setDescription("Test Description");
        article.setAuthor("Test Author");
        article.setContent("Test Content");
        article.setCreated(LocalDateTime.now());
        article.setVisible(visible);
        return article;
    }

    public static Article createArticleWithSanitizedTitle(String title, boolean visible) {
        Article article = createArticle(title, visible);
        article.setSanitizedTitle(article.getTitle().replaceAll("([\\W]+)", "-").toLowerCase());
        return article;
    }

    public static Article createVisibleArticle() {
        return createArticle(String.format("Test Article %d", counter), true);
    }

    public static Article createNotVisibleArticle() {
        return createArticle(String.format("Test Article %d", counter), false);
    }

    public static Tag createTag(String name) {
        Tag tag = new Tag();
        tag.setName(name);
        tag.setColor("#000000");
        return tag;
    }

}
