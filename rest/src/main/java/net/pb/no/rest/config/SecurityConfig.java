package net.pb.no.rest.config;

import net.pb.no.rest.filter.CORSFilter;
import net.pb.no.rest.filter.SecurityAuthenticationFilter;
import net.pb.no.rest.filter.SecurityLoginFilter;
import net.pb.no.service.api.security.SecurityTokenService;
import net.pb.no.service.impl.security.SecurityTokenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.nio.charset.StandardCharsets;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    // Stored in env provided by server context
    @Value("#{environment.jwtSecret}")
    private String jwtSecret;

    @Value("#{environment.adminUsername}")
    private String adminUsername;

    @Value("#{environment.adminPassword}")
    private String adminPassword;

    public static final String AUTH_HEADER = "X-AUTH-TOKEN";
    public static final String CLAIMS_KEY = "credentials";

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser(adminUsername).password(adminPassword).roles("ADMIN");
        auth.eraseCredentials(false);
    }

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public SecurityTokenService securityTokenService() {
        return new SecurityTokenServiceImpl(jwtSecret);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding(StandardCharsets.UTF_8.name());

        AuthenticationManager authenticationManager = authenticationManager();
        SecurityTokenService securityTokenService = securityTokenService();

        http.addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class)
                .addFilterBefore(new SecurityLoginFilter("/auth/login", authenticationManager, securityTokenService),
                        UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new SecurityAuthenticationFilter(authenticationManager, securityTokenService),
                        UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .anyRequest().permitAll()
                .and()
                .csrf().disable();
    }
}