package net.pb.no.rest.controller;

import net.pb.no.modelui.UiEmbedded;
import net.pb.no.service.api.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/articles")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/searchByTitle", method = RequestMethod.GET, produces = MediaTypes.HAL_JSON_VALUE)
    public UiEmbedded searchByTitle(@RequestParam("title") String title) {
        UiEmbedded results = new UiEmbedded(searchService.searchByTitle(title));
        results.add(linkTo(methodOn(SearchController.class).searchByTitle(title)).withSelfRel());
        return results;
    }

}
