package net.pb.no.rest.controller;

import net.pb.no.model.jaxb.rss.TRss;
import net.pb.no.model.jaxb.sitemap.Urlset;
import net.pb.no.service.api.site.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class SiteController {

    @Autowired
    private SiteService siteService;

    @RequestMapping(value = "/sitemap", produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public Urlset generateSitemap() {
        return siteService.generateSitemap();
    }

    @RequestMapping(value = "/rss", produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public TRss generateRss() {
        return siteService.generateRss();
    }

}
