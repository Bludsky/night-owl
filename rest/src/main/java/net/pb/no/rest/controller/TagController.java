package net.pb.no.rest.controller;

import net.pb.no.modelui.UiEmbedded;
import net.pb.no.modelui.tag.UiTag;
import net.pb.no.service.api.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/tags")
public class TagController {

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/aggregated", method = RequestMethod.GET, produces = MediaTypes.HAL_JSON_VALUE)
    public ResponseEntity<UiEmbedded> getAggregatedTags() {
        List<UiTag> tagList = tagService.aggregateTags();
        UiEmbedded tags = new UiEmbedded(tagList);
        tags.add(linkTo(methodOn(TagController.class).getAggregatedTags()).withSelfRel());

        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

}
