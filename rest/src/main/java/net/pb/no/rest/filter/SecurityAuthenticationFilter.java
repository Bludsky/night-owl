package net.pb.no.rest.filter;

import net.pb.no.model.security.UserCredentials;
import net.pb.no.rest.config.SecurityConfig;
import net.pb.no.service.api.security.SecurityTokenService;
import net.pb.no.service.api.security.exception.SecurityTokenException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

public class SecurityAuthenticationFilter extends GenericFilterBean {

    private AuthenticationManager authenticationManager;
    private SecurityTokenService securityTokenService;

    public SecurityAuthenticationFilter(AuthenticationManager authenticationManager, SecurityTokenService securityTokenService) {
        this.authenticationManager = authenticationManager;
        this.securityTokenService = securityTokenService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        final String token = ((HttpServletRequest) request).getHeader(SecurityConfig.AUTH_HEADER);
        if (token != null) {
            Object claimsObj = securityTokenService.readToken(token, SecurityConfig.CLAIMS_KEY);

            if (!(claimsObj instanceof Map)) {
                throw new SecurityTokenException("Incoming token is malformed. Not an instance of Map");
            }

            Map<String, String> credentialsMap = (Map<String, String>) claimsObj;
            final String username = credentialsMap.get("username");
            final String password = credentialsMap.get("password");

            if (username == null || password == null) {
                throw new SecurityTokenException("Token conveys incomplete information");
            }

            UserCredentials credentials = new UserCredentials(username, password);
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);
            authToken.setDetails(credentials);

            Authentication authenticate = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authenticate);
        }
        filterChain.doFilter(request, response);
    }
}
