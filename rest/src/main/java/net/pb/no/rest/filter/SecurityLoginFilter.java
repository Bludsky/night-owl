package net.pb.no.rest.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.pb.no.model.security.UserCredentials;
import net.pb.no.rest.config.SecurityConfig;
import net.pb.no.service.api.security.SecurityTokenService;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Stateless implementation of JWT based security
 */
public class SecurityLoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private SecurityTokenService securityTokenService;

    public SecurityLoginFilter(String mapping, AuthenticationManager authenticationManager, SecurityTokenService securityTokenService) {
        super(new AntPathRequestMatcher(mapping));
        this.securityTokenService = securityTokenService;
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {
        UserCredentials userCredentials = MAPPER.readValue(request.getInputStream(), UserCredentials.class);
        final String username = userCredentials.getUsername();
        final String password = userCredentials.getPassword();
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        return getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException {
        Object principal = authResult.getPrincipal();
        Assert.isInstanceOf(User.class, principal);
        User user = (User) principal;
        UserCredentials credentials = new UserCredentials(user.getUsername(), user.getPassword());
        final String token = securityTokenService.createToken(credentials, SecurityConfig.CLAIMS_KEY);
        response.setHeader(SecurityConfig.AUTH_HEADER, token);

        // Superagent workaround
        response.setHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
        response.setHeader("success", "yes");
        PrintWriter writer = response.getWriter();
        writer.write(token);
        writer.close();
    }

}
