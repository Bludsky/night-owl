package net.pb.no.rest.test;

import net.pb.no.rest.test.config.TestRestConfig;
import net.pb.repository.test.AbstractRepositoryTest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = TestRestConfig.class)
public abstract class AbstractRestWithRepositoryTest extends AbstractRepositoryTest {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRepositoryTest.class);

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeClass
    public static void setUpBeforeClass() {
        // Set security env properties
        System.setProperty("adminUsername", "user");
        System.setProperty("adminPassword", "password");
        System.setProperty("jwtSecret", "testSecret");
    }

    @Before
    public void setUp() {
        setUpMvc();
        cleanRepositories();
    }

    protected void setUpMvc() {
        LOG.info("Setting up mock mvc");
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

}
