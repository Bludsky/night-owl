package net.pb.no.rest.test;

import net.pb.no.model.mongodb.Article;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.nio.charset.StandardCharsets;

import static net.pb.no.rest.test.utils.JSONUtils.readJsonResource;
import static net.pb.repository.test.util.RepositoryTestModelFactory.createArticleWithSanitizedTitle;
import static net.pb.repository.test.util.RepositoryTestModelFactory.createVisibleArticle;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ArticleRestWithRepositoryTest extends AbstractRestWithRepositoryTest {

    // Resources
    static final String POST_ARTICLE_JSON = "/json/postArticle.json";
    static final String PATCH_ARTICLE_JSON = "/json/patchArticle.json";
    Logger LOG = LoggerFactory.getLogger(ArticleRestWithRepositoryTest.class);

    @Test
    public void getAllArticlesTest() throws Exception {
        mockMvc.perform(get("/articles"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @Ignore
    public void getArticleTest() throws Exception {
        mockMvc.perform(get("/articles/id"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void getAllVisibleArticlesTest() throws Exception {
        articleRepository.save(createVisibleArticle());

        String contentAsString = mockMvc.perform(get("/articles/search/findByVisibleIsTrue"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$..articles", hasSize(1)))
                .andReturn()
                .getResponse()
                .getContentAsString();

        LOG.info(contentAsString);
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void articlePaginationTest() throws Exception {
        articleRepository.save(createArticleWithSanitizedTitle("Test Article 1", true));
        articleRepository.save(createArticleWithSanitizedTitle("Test Article 2", true));
        articleRepository.save(createArticleWithSanitizedTitle("Test Article 3", true));

        String contentAsString = mockMvc.perform(get("/articles/search/findByVisibleIsTrue")
                .param("page", "0")
                .param("size", "10")
                .param("order", "title"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.articles", hasSize(3)))
                .andExpect(jsonPath("$.page.totalElements", is(3)))
                .andReturn()
                .getResponse()
                .getContentAsString();

        LOG.info(contentAsString);
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void getArticleByIdTest() throws Exception {
        Article article = articleRepository.save(createVisibleArticle());

        mockMvc.perform(get("/articles/" + article.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", instanceOf(String.class)))
                .andExpect(jsonPath("$.description", instanceOf(String.class)))
                .andExpect(jsonPath("$.author", instanceOf(String.class)))
                .andExpect(jsonPath("$.created", instanceOf(java.util.LinkedHashMap.class)))
                .andExpect(jsonPath("$.visible", is(true)))
                .andExpect(jsonPath("$.content", instanceOf(String.class)));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void postArticleTest() throws Exception {
        final String sanitizedTitle = "test-title";

        String json = readJsonResource(POST_ARTICLE_JSON);
        mockMvc.perform(post("/articles")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(json))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/articles/search/findBySanitizedTitleAndVisibleIsTrue")
                .param("sanitizedTitle", sanitizedTitle))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sanitizedTitle", is(sanitizedTitle)));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void patchArticleTest() throws Exception {
        String json = readJsonResource(PATCH_ARTICLE_JSON);

        // Save an article, obtain its id
        Article article = articleRepository.save(createVisibleArticle());

        // Patch its title
        mockMvc.perform(patch("/articles/" + article.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(json))
                .andExpect(status().isNoContent());

        // Verify it's patched
        mockMvc.perform(get("/articles/" + article.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("Patched Title")));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void deleteArticleTest() throws Exception {
        Article article = articleRepository.save(createVisibleArticle());

        Assert.assertThat(articleRepository.count(), is(1L));

        mockMvc.perform(delete("/articles/" + article.getId()))
                .andExpect(status().isNoContent());

        Assert.assertThat(articleRepository.count(), is(0L));
    }

}
