package net.pb.no.rest.test;

import net.pb.no.modelui.search.UiSearchResult;
import net.pb.no.service.api.search.SearchService;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.IsNot.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = SearchControllerTest.Config.class)
public class SearchControllerTest extends AbstractRestWithRepositoryTest {

    @Autowired
    private SearchService searchService;

    @Test
    public void getSearchResultsTest() throws Exception {
        UiSearchResult result = new UiSearchResult("title", "sanitizedTitle");
        Mockito.when(searchService.searchByTitle("title"))
                .thenReturn(
                        Collections.singletonList(result)
                );

        mockMvc.perform(get("/articles/searchByTitle")
                .param("title", "title"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", not(empty())))
                .andExpect(jsonPath("$.content[0].title", is(result.getTitle())))
                .andExpect(jsonPath("$.content[0].sanitizedTitle", is(result.getSanitizedTitle())));
    }

    @Configuration
    static class Config {

        @Bean
        public SearchService searchService() {
            return Mockito.mock(SearchService.class);
        }

    }

}
