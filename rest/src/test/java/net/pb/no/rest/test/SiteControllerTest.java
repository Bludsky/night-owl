package net.pb.no.rest.test;

import net.pb.no.model.jaxb.rss.TRss;
import net.pb.no.model.jaxb.rss.TRssChannel;
import net.pb.no.model.jaxb.rss.TRssItem;
import net.pb.no.model.jaxb.sitemap.TChangeFreq;
import net.pb.no.model.jaxb.sitemap.TUrl;
import net.pb.no.model.jaxb.sitemap.Urlset;
import net.pb.no.service.api.site.SiteService;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = SiteControllerTest.Config.class)
public class SiteControllerTest extends AbstractRestWithRepositoryTest {

    public static final Logger LOG = LoggerFactory.getLogger(SiteControllerTest.class);

    @Autowired
    private SiteService siteService;

    @Test
    public void generateSitemapTest() throws Exception {
        Urlset urlset = new Urlset();
        TUrl tUrl = new TUrl();
        tUrl.setLoc("http://www.nightowl.it");
        tUrl.setChangefreq(TChangeFreq.ALWAYS);
        tUrl.setLastmod("last modification");
        tUrl.setPriority(BigDecimal.valueOf(0.5));
        urlset.getAny().add(tUrl);

        Mockito.when(siteService.generateSitemap()).thenReturn(urlset);

        String contentAsString = mockMvc.perform(get("/sitemap"))
                .andExpect(content().contentType(MediaType.APPLICATION_XML))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LOG.info(contentAsString);
    }

    @Test
    public void generateRssTest() throws Exception {
        TRss rss = new TRss();
        rss.setVersion(BigDecimal.valueOf(2.0));
        TRssChannel rssChannel = new TRssChannel();
        rssChannel.setTitle("Night Owl");
        rssChannel.setLink("http://www.nightowl.it");
        rssChannel.setDescription("Software development blog");
        rssChannel.setLanguage("en-us");
        rss.getChannel().add(rssChannel);

        TRssItem rssItem = new TRssItem();
        rssItem.setTitle("About Night Owl");
        rssItem.setLink("http://www.nightowl.it/about");
        rssItem.setDescription("About Night Owl blog");
        rssChannel.getItem().add(rssItem);

        Mockito.when(siteService.generateRss()).thenReturn(rss);

        String contentAsString = mockMvc.perform(get("/rss"))
                .andExpect(content().contentType(MediaType.APPLICATION_XML))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LOG.info(contentAsString);
    }

    @Configuration
    static class Config {

        @Bean
        public SiteService siteService() {
            return Mockito.mock(SiteService.class);
        }

    }

}
