package net.pb.no.rest.test;


import net.pb.no.modelui.tag.UiTag;
import net.pb.no.service.api.tag.TagService;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = TagControllerTest.Config.class)
public class TagControllerTest extends AbstractRestWithRepositoryTest {

    @Autowired
    private TagService tagService;

    @Test
    public void getAggregatedTagsTest() throws Exception {
        List<UiTag> uiTags = Arrays.asList(
                new UiTag("Python", "#000000", 3),
                new UiTag("Java", "#000000", 1)

        );

        Mockito.when(tagService.aggregateTags()).thenReturn(uiTags);
        mockMvc.perform(get("/tags/aggregated"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].name", is("Python")))
                .andExpect(jsonPath("$.content[0].color", is("#000000")))
                .andExpect(jsonPath("$.content[0].occurrence", is(3)))
                .andExpect(jsonPath("$.content[1].name", is("Java")))
                .andExpect(jsonPath("$.content[1].color", is("#000000")))
                .andExpect(jsonPath("$.content[1].occurrence", is(1)));
    }

    @Configuration
    static class Config {

        @Bean
        public TagService tagService() {
            return Mockito.mock(TagService.class);
        }

    }

}
