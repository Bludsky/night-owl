package net.pb.no.rest.test.config;

import net.pb.no.rest.config.RestConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(RestConfig.class)
public class TestRestConfig {

}
