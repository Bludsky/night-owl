package net.pb.no.rest.test.utils;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public final class JSONUtils {

    private JSONUtils() {
    }

    public static String readJsonResource(String path) throws IOException {
        ClassPathResource resource = new ClassPathResource(path);
        return IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8.toString());
    }

}
