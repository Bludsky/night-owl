package net.pb.no.service.api.search;

import net.pb.no.modelui.search.UiSearchResult;

import java.util.List;

public interface SearchService {
    /**
     * Regex based search for titles and sanitized titles of articles by {@param str}
     *
     * @param phrase search phrase
     * @return {@link UiSearchResult} containing
     */
    List<UiSearchResult> searchByTitle(String phrase);
}
