package net.pb.no.service.api.security;

public interface SecurityTokenService {
    /**
     * Creates and signs json web token
     *
     * @param object claims to be inserted into token
     * @param key key claims are stored under
     * @return signed JWT
     */
    String createToken(Object object, String key);

    /**
     * Parse token a returns inserted claims
     *
     * @param token string representation of token
     * @param key key claims are stored under
     * @return claims
     */
    Object readToken(String token, String key);
}
