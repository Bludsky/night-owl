package net.pb.no.service.api.security.exception;

public class SecurityTokenException extends RuntimeException {

    public SecurityTokenException(String message) {
        super(message);
    }

    public SecurityTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
