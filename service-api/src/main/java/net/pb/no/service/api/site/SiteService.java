package net.pb.no.service.api.site;

import net.pb.no.model.jaxb.rss.TRss;
import net.pb.no.model.jaxb.sitemap.Urlset;

public interface SiteService {

    /**
     * Generate xml sitemap using Google schema
     *
     * @return
     */
    Urlset generateSitemap();

    /**
     * Generate RSS 2.0 feed
     *
     * @return
     */
    TRss generateRss();
}
