package net.pb.no.service.api.tag;

import net.pb.no.modelui.tag.UiTag;

import java.util.List;

public interface TagService {
    /**
     * Get a list of aggregated tags
     *
     * @return list of sum aggregated tags by name and color
     */
    List<UiTag> aggregateTags();
}
