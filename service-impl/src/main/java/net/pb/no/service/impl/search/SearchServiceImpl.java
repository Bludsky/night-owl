package net.pb.no.service.impl.search;

import net.pb.no.model.mongodb.Article;
import net.pb.no.model.mongodb.ArticleSimpleListProjection;
import net.pb.no.modelui.search.UiSearchResult;
import net.pb.no.service.api.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO replace with Spring Data projection ({@link ArticleSimpleListProjection})
 */
@Service
public class SearchServiceImpl implements SearchService {

    private static final String FIELD_TITLE = "title";
    private static final String FIELD_VISIBLE = "visible";

    private static final int QUERY_LIMIT = 30;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<UiSearchResult> searchByTitle(String phrase) {
        Query query = new Query();
        query.limit(QUERY_LIMIT);
        query.addCriteria(Criteria
                .where(FIELD_TITLE)
                .regex(phrase, "i")
                .and(FIELD_VISIBLE)
                .is(true));
        List<Article> articles = mongoTemplate.find(query, Article.class);

        return articles.stream()
                .map(article -> new UiSearchResult(article.getTitle(), article.getSanitizedTitle()))
                .collect(Collectors.<UiSearchResult>toList());
    }

}
