package net.pb.no.service.impl.security;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import net.pb.no.service.api.security.SecurityTokenService;
import net.pb.no.service.api.security.exception.SecurityTokenException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

public class SecurityTokenServiceImpl implements SecurityTokenService {

    private JWTSigner signer;
    private JWTVerifier verifier;
    private Integer expirySeconds;

    public SecurityTokenServiceImpl(String secret) {
        signer = new JWTSigner(secret);
        verifier = new JWTVerifier(secret);
        expirySeconds = 60 * 60 * 12;
    }

    @Override
    public String createToken(Object object, String key) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(key, object);
        return signer.sign(claims, new JWTSigner.Options().setExpirySeconds(expirySeconds));
    }

    @Override
    public Object readToken(String token, String key) {
        try {
            Map<String, Object> claims = verifier.verify(token);
            return claims.get(key);
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | SignatureException | JWTVerifyException e) {
            throw new SecurityTokenException("Unable to decipher supplied security token", e);
        }
    }

    public void setExpirySeconds(Integer expirySeconds) {
        this.expirySeconds = expirySeconds;
    }

}
