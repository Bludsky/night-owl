package net.pb.no.service.impl.site;

import net.pb.no.model.jaxb.rss.TGuid;
import net.pb.no.model.jaxb.rss.TRss;
import net.pb.no.model.jaxb.rss.TRssChannel;
import net.pb.no.model.jaxb.rss.TRssItem;
import net.pb.no.model.jaxb.sitemap.TChangeFreq;
import net.pb.no.model.jaxb.sitemap.TUrl;
import net.pb.no.model.jaxb.sitemap.Urlset;
import net.pb.no.model.mongodb.Article;
import net.pb.no.service.api.site.SiteService;
import net.pb.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Service
public class SiteServiceImpl implements SiteService {

    public static final DateTimeFormatter DFM = DateTimeFormatter.ofPattern("E, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

    @Value("${sitemap.host}")
    private String host;

    @Value("${rss.title}")
    private String rssTitle;

    @Value("${rss.link}")
    private String rssLink;

    @Value("${rss.description}")
    private String rssDescription;

    @Value("${rss.language}")
    private String rssLanguage;

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public Urlset generateSitemap() {
        Urlset sitemap = new Urlset();
        addStaticPages(sitemap);
        addArticlesAndAuthors(sitemap);

        return sitemap;
    }

    @Override
    public TRss generateRss() {
        TRss rss = new TRss();
        rss.setVersion(BigDecimal.valueOf(2.0));
        rss.getChannel().add(createRssChannel());

        return rss;
    }

    private void addStaticPages(Urlset sitemap) {
        List<TUrl> urls = sitemap.getUrl();

        // Home
        urls.add(createUrl(host, TChangeFreq.DAILY));
        // About
        urls.add(createUrl(host.concat("about"), TChangeFreq.DAILY));
    }

    private void addArticlesAndAuthors(Urlset sitemap) {
        List<TUrl> urls = sitemap.getUrl();

        Set<String> authors = new HashSet<>();
        articleRepository.findByVisibleIsTrueOrderByCreated()
                .forEach(a -> {
                    urls.add(
                            createUrl(host.concat(a.getSanitizedTitle()), TChangeFreq.WEEKLY)
                    );
                    authors.add(a.getSanitizedAuthor());
                });

        authors.forEach(a ->
                urls.add(
                        createUrl(host.concat("author/" + a), TChangeFreq.DAILY)
                ));
    }

    private TUrl createUrl(String location, TChangeFreq changeFreq) {
        TUrl tUrl = new TUrl();
        tUrl.setLoc(location);
        tUrl.setChangefreq(changeFreq);

        return tUrl;
    }

    private TRssChannel createRssChannel() {
        TRssChannel rssChannel = new TRssChannel();
        rssChannel.setTitle(rssTitle);
        rssChannel.setLink(rssLink);
        rssChannel.setDescription(rssDescription);
        rssChannel.setLanguage(rssLanguage);
        rssChannel.setLastBuildDate(ZonedDateTime.now().format(DFM));

        articleRepository.findByVisibleIsTrueOrderByCreated()
                .forEach(a -> rssChannel.getItem().add(createRssItem(a)));

        return rssChannel;
    }

    private TRssItem createRssItem(Article article) {
        TRssItem rssItem = new TRssItem();
        rssItem.setTitle(article.getTitle());
        rssItem.setLink(rssLink.concat("/" + article.getSanitizedTitle()));
        TGuid guid = new TGuid();
        guid.setValue(rssLink.concat("/" + article.getSanitizedTitle()));
        guid.setIsPermaLink(true);
        rssItem.setGuid(guid);
        rssItem.setDescription(article.getDescription());
        ZonedDateTime zonedDateTime = ZonedDateTime.of(article.getCreated(), ZoneId.of("UTC"));
        rssItem.setPubDate(zonedDateTime.format(DFM));

        return rssItem;
    }

}
