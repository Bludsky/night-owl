package net.pb.no.service.impl.tag;

import net.pb.no.model.mongodb.Article;
import net.pb.no.modelui.tag.UiTag;
import net.pb.no.service.api.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
public class TagServiceImpl implements TagService {

    private static final String FIELD_TAGS = "tags";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_COLOR = "color";
    private static final String FIELD_OCCURRENCE = "occurrence";
    private static final String FIELD_VISIBLE = "visible";

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<UiTag> aggregateTags() {
        Aggregation aggregation = Aggregation.newAggregation(
                match(Criteria.where(FIELD_VISIBLE).is(true)),
                project(FIELD_TAGS),
                unwind(FIELD_TAGS),
                group(FIELD_TAGS + "." + FIELD_NAME, FIELD_TAGS + "." + FIELD_COLOR)
                        .count()
                        .as(FIELD_OCCURRENCE),
                project(FIELD_OCCURRENCE)
                        .and(FIELD_NAME)
                        .nested(bind(FIELD_NAME, FIELD_TAGS + "." + FIELD_NAME))
                        .and(FIELD_COLOR)
                        .nested(bind(FIELD_COLOR, FIELD_TAGS + "." + FIELD_COLOR)),
                sort(Sort.Direction.DESC, FIELD_OCCURRENCE));

        return mongoTemplate.aggregate(aggregation, Article.class, UiTag.class).getMappedResults();
    }

}
