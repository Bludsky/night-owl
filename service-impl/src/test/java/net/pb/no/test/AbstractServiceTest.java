package net.pb.no.test;

import net.pb.no.test.conf.TestServiceConfig;
import net.pb.repository.test.AbstractRepositoryTest;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfig.class)
public abstract class AbstractServiceTest extends AbstractRepositoryTest {
}
