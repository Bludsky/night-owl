package net.pb.no.test;

import net.pb.no.model.mongodb.Article;
import net.pb.no.modelui.search.UiSearchResult;
import net.pb.no.service.api.search.SearchService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static net.pb.repository.test.util.RepositoryTestModelFactory.createArticleWithSanitizedTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.IsNot.not;

public class SearchServiceTest extends AbstractServiceTest {

    @Autowired
    private SearchService searchService;

    @Test
    public void searchByTitleTest() {
        Article articleOne = createArticleWithSanitizedTitle("rEgEx title", true);
        Article articleTwo = createArticleWithSanitizedTitle("rEgEx title 2", false);
        articleRepository.save(articleOne);
        articleRepository.save(articleTwo);

        List<UiSearchResult> results = searchService.searchByTitle("ege");
        assertThat(results, not(empty()));
    }

}
