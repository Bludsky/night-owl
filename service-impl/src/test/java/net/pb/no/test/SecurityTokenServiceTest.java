package net.pb.no.test;

import net.pb.no.model.security.UserCredentials;
import net.pb.no.service.api.security.SecurityTokenService;
import net.pb.no.service.impl.security.SecurityTokenServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SecurityTokenServiceTest.SecurityConfiguration.class)
public class SecurityTokenServiceTest {

    @Autowired
    private SecurityTokenService service;

    @Test
    public void securityRoundTripTest() {
        final String username = "username";
        final String password = "password";
        final String key = "credentials";

        UserCredentials credentials = new UserCredentials(username, password);
        String token = service.createToken(credentials, key);
        assertThat(token, is(notNullValue()));

        Object credentialsObj = service.readToken(token, key);
        assertThat(credentialsObj, instanceOf(Map.class));
        Map<String, String> credentialsMap = (Map<String, String>) credentialsObj;
        assertThat(credentialsMap.get("username"), is(username));
        assertThat(credentialsMap.get("password"), is(password));
    }

    @Configuration
    static class SecurityConfiguration {
        @Bean
        public SecurityTokenService securityTokenService() {
            return new SecurityTokenServiceImpl("secret");
        }
    }

}
