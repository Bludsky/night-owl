package net.pb.no.test;

import net.pb.no.model.mongodb.Article;
import net.pb.no.model.mongodb.Tag;
import net.pb.no.modelui.tag.UiTag;
import net.pb.no.service.api.tag.TagService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static net.pb.repository.test.util.RepositoryTestModelFactory.createArticleWithSanitizedTitle;
import static net.pb.repository.test.util.RepositoryTestModelFactory.createTag;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class TagServiceTest extends AbstractServiceTest {

    @Autowired
    private TagService tagService;

    @Test
    public void aggregateTagsTest() {
        Article articleOne = createArticleWithSanitizedTitle("Test Article 1", true);
        Article articleTwo = createArticleWithSanitizedTitle("Test Article 2", true);
        Article articleThree = createArticleWithSanitizedTitle("Test Article 3", true);

        final String tagJava = "Java";
        final String tagSpring = "Spring";
        final String tagWS = "WS";

        Tag tag = createTag(tagJava);
        Tag tag2 = createTag(tagSpring);
        Tag tag3 = createTag(tagWS);

        List<Tag> tags = Arrays.asList(tag);
        List<Tag> tags2 = Arrays.asList(tag, tag2);
        List<Tag> tags3 = Arrays.asList(tag, tag2, tag3);

        articleOne.setTags(tags);
        articleTwo.setTags(tags2);
        articleThree.setTags(tags3);

        articleRepository.save(articleOne);
        articleRepository.save(articleTwo);
        articleRepository.save(articleThree);

        List<UiTag> uiTags = tagService.aggregateTags();
        assertThat(uiTags, hasSize(3));
        assertThat(uiTags.get(0).getName(), is(tagJava));
        assertThat(uiTags.get(0).getOccurrence(), equalTo(Integer.valueOf(3)));
        assertThat(uiTags.get(1).getName(), is(tagSpring));
        assertThat(uiTags.get(1).getOccurrence(), equalTo(Integer.valueOf(2)));
        assertThat(uiTags.get(2).getName(), is(tagWS));
        assertThat(uiTags.get(2).getOccurrence(), equalTo(Integer.valueOf(1)));

    }

}
