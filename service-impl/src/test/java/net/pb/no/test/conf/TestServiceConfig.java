package net.pb.no.test.conf;

import net.pb.no.service.config.ServiceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(ServiceConfig.class)
@Configuration
public class TestServiceConfig {

}
